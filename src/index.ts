import express from "express";
import { urlencoded, json } from "body-parser";
import dotenv from "dotenv";
import { Server, ServerOptions, Socket } from "socket.io";
import { createSocketServer } from "./utils/socketServer";
// const http = require('http');

//Express server

dotenv.config();
const app = express();

const port = process.env.PORT || 3333;
app.use(express.json());
app.use(express.urlencoded());
app.use(urlencoded({ extended: true }));
app.use(json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

const server = app.listen(port, () => {
  console.log(`SocketIO server is running on port ${port}`);
});

//Socket server
const serverOptions: Partial<ServerOptions> = {
  path: "",
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
  maxHttpBufferSize: 3e7,
  pingTimeout: 60000,
  transports: ['websocket', 'polling'],
  allowEIO3: true
};
const io = createSocketServer(server, serverOptions);

const router = express.Router();

router.get("/room", (req, res) => {
  res.status(200).json({data: 'TEST SERVER'});
});


app.use("/socket-server", router);
