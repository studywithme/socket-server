import { callApi } from "../utils/axios";
import { getErrorResponse, Payload, Response } from "../actions/data.constant";
import { API_ROOM_PATH } from "../constants/room.constants";

export const getAllRoom = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "GET",
      API_ROOM_PATH.ROOM,
      payload.options
    );
    // TODO 
  } catch (err: any) {
    callback(getErrorResponse(err));
    // logger.error('getAllNotification error:', err);
  }
};

export const getRoomById = async (
  token: string,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "GET",
      API_ROOM_PATH.ROOM,
      payload.options
    );
  } catch (err: any) {

  }
}

export const createNewRoom = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "POST",
      API_ROOM_PATH.NEW_ROOM,
      payload.options,
      payload.data
    );
  } catch (err: any) {}
}

export const joinRoom = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "POST",
      '/room/' + payload.data.roomId + '/join',
      payload.options,
      payload.data
    );
    callback(res);
  } catch (err: any) {
    callback(null);
  }
}

export const leftRoom = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "POST",
      '/room/' + payload.data.roomId + '/left',
      payload.options,
      payload.data
    );
    callback(res);
  } catch (err: any) {
    callback(null);
  }
}


export const breakTime = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "PUT",
      '/room/' + payload.data.roomId + '/break',
      payload.options,
      payload.data
    );
    callback(res);
  } catch (err: any) {
    callback(null);
  }
}


export const overBreak = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "PUT",
      '/room/' + payload.data.roomId + '/over-break',
      payload.options,
      payload.data
    );
    callback(res);
  } catch (err: any) {
    callback(null);
  }
}

export const joinRoomSingle = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "POST",
      '/room/join-single',
      payload.options,
      payload.data
    );
    callback(res);
  } catch (err: any) {
    callback(null);
  }
}

export const leftRoomSingle = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "POST",
      '/room/left-single',
      payload.options,
      payload.data
    );
    callback(res);
  } catch (err: any) {
    callback(null);
  }
}

export const changeSettings = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "PUT",
      '/room/change-setting',
      payload.options,
      payload.data
    );
    callback(res);
  } catch (err: any) {
    callback(null);
  }
}