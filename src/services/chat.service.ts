import { callApi } from "../utils/axios";
import { getErrorResponse, Payload, Response } from "../actions/data.constant";
import { API_CHAT_PATH } from "../constants/chat.constant";

// export const getAllMessageInRoom = async (
//   token: string,
//   payload: Payload,
//   callback: (res: Response<any>) => void
// ): Promise<void> => {
//   try {
//     const res: Response<any> = await callApi(
//       token,
//       "GET",
//       API_ROOM_PATH.ROOM,
//       payload.options
//     );
//     // TODO 
//   } catch (err: any) {
//     callback(getErrorResponse(err));
//     // logger.error('getAllNotification error:', err);
//   }
// };

export const sendMessage = async (
  token: any,
  payload: Payload,
  callback: (res: Response<any>) => void
): Promise<void> => {
  try {
    const res: Response<any> = await callApi(
      token,
      "POST",
      'chat/sendMessage',
      payload.options,
      payload.data
    );
    console.log(JSON.stringify(res));
    
    callback(res);
  } catch (err: any) {
    console.log('ERROR', err);
    
    callback(null);
  }
}
