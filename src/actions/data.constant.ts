export interface Payload {
  data?: any;
  options?: any;
}

export type Error = {
  code: string;
  message: string;
};

export type Response<T = any> =
  | {
      data: T;
      success: boolean;
      error: Array<Error>;
      validateMessage?: any;
    }
  | T;

export const getErrorResponse = (message: string): Response<any> => {
  const failed: Response<any> = {
    data: null,
    error: [
      {
        code: "400 - SERVER ERROR",
        message,
      },
    ],
    success: false,
  };
  return failed;
};
