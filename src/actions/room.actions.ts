import { Payload, Response } from "./data.constant";

export const ROOM_SPACE = 'ROOM_SP';



export const ROOM_ACTIONS = {
    LIST_ROOM: 'ROOM:LIST',
    ROOM_INFO: 'ROOM:INFO',
    USER_JOINED: 'ROOM:USER_JOINED',
    USER_LEFT: 'ROOM:USER_LEFT'
}

export interface RoomServerEvents {
}

export interface RoomClientEvents {
    'room:add': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room:get_list': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room:list_room': (payload: String) => void;
    'room:info': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room:join': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room:break': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room:break_success': (payload: String) => void;
    'room:over_break': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room:over_break_success': (payload: String) => void;
    'room:left': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room_single:join': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room_single:left': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room:user_joined': (payload: String) => void;
    'room:user_left': (payload: String) => void;
    'room:join_success': (payload: String) => void;
    'room:join_fail': (payload: String) => void;
    'room:change_setting': (payload: Payload, callback: (res: Response<any>) => void) => void;
    'room:user_change_setting': (payload: String) => void;
}

