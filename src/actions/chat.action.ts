import { Payload, Response } from "./data.constant";

export const CHAT_SPACE = 'ROOM_SP';


export interface ChatServerEvents {
}

export interface ChatClientEvents {
    'chat:join_room': (payload: Payload) => void;
    'chat:left_room': (payload: Payload) => void;
    'chat:send_message': (payload: Payload) => void;
    'chat:user_send_message': (payload: String) => void;
}

