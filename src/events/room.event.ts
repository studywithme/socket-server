import { Payload, Response } from "./../actions/data.constant";
import { Socket, Server } from "socket.io";
import { RoomClientEvents } from "../actions/room.actions";
import { breakTime, changeSettings, createNewRoom, getAllRoom, getRoomById, joinRoom, joinRoomSingle, leftRoom, leftRoomSingle, overBreak } from "../services/room.service";

export const RoomEvent = (
  socket: Socket<RoomClientEvents>,
  io: Server
) => {
  const token = socket.handshake.query['token'] || '';
  socket.on(
    "room:add", (payload: Payload, callback: (res: Response<any>) => void) => {
      console.log(payload);
      createNewRoom(token, payload, callback);
    }
  )
  socket.on(
    "room:get_list",
    (payload: Payload, callback: (res: Response<any>) => void) => {
      getAllRoom(token, payload, (res: any) => {
        socket.emit('room:list_room', JSON.stringify(res));
      });
    }
  );
  // socket.on("room:info",
  //   (payload: Payload, callback: (res: Response<any>) => void) => {
  //     getRoomById(token, payload, callback);
  //   })
  socket.on("room:change_setting",
    (payload: Payload, callback: (res: Response<any>) => void) => {
      changeSettings(token, payload, (res: any) => {
        if (res != null) {
          const { roomId } = payload.data;
          let roomName = 'room' + roomId;
          socket.broadcast.to(roomName).emit('room:user_change_setting', JSON.stringify({ ...res }));
        } else {
          //socket.emit('room:join_fail', '')
        }
      });
    });
  socket.on("room:join",
    (payload: Payload, callback: (res: Response<any>) => void) => {
      joinRoom(token, payload, (res: any) => {
        if (res != null) {
          const { roomId } = payload.data;
          let roomName = 'room' + roomId;
          // join to room
          socket.join(roomName);
          // emit to all user on room
          socket.emit('room:join_success', JSON.stringify({ ...res }))
          socket.broadcast.to(roomName).emit('room:user_joined', JSON.stringify({ ...res }));
        } else {
          socket.emit('room:join_fail', '')
        }
      });
    })

  socket.on("room:left",
    (payload: Payload, callback: (res: Response<any>) => void) => {
      leftRoom(token, payload, (res: any) => {
        console.log('Socket room:left', res);
        if (res != null) {
          const { roomId } = payload.data;
          let roomName = 'room' + roomId;
          // join to room
          socket.leave(roomName);
          // emit to all user on room
          socket.broadcast.to(roomName).emit('room:user_left', JSON.stringify({
            ...res
          }));
        }
      });
    })

  socket.on("room_single:join",
    (payload: Payload, callback: (res: Response<any>) => void) => {
      joinRoomSingle(token, payload, (res: any) => { });
    })

  socket.on("room_single:left",
    (payload: Payload, callback: (res: Response<any>) => void) => {
      leftRoomSingle(token, payload, (res: any) => { });
    })

  socket.on("room:break",
    (payload: Payload, callback: (res: Response<any>) => void) => {
      breakTime(token, payload, (res: any) => {
        if (res != null) {
          // emit to all user on room
          socket.emit('room:break_success', JSON.stringify({ ...res }))
        } else {
        }
      });
    })

  socket.on("room:over_break",
    (payload: Payload, callback: (res: Response<any>) => void) => {
      overBreak(token, payload, (res: any) => {
        console.log('Socket room:left', res);
        if (res != null) {
          socket.emit('room:over_break_success', JSON.stringify({ ...res }))
        }
      });
    })
};

