import { Payload, Response } from "./../actions/data.constant";
import { Socket, Server } from "socket.io";
import { changeSettings, createNewRoom, getAllRoom, getRoomById, joinRoom, leftRoom } from "../services/room.service";
import { ChatClientEvents } from "../actions/chat.action";
import { sendMessage } from "../services/chat.service";


export const ChatEvent = (
  socket: Socket<ChatClientEvents>,
  io: Server
) => {
   const token = socket.handshake.query['token'] || '';
  //const token = socket.handshake.auth.token;
  socket.on("chat:join_room",
    (payload: Payload) => {
      console.log("TOKEN", token);
      
      const { roomId, type } = payload.data;
      let roomName = 'room_chat_' + type +'_'+ roomId;
      // if (type == 'single') {
      //   const {idUser, target} = payload.data;
      //   roomName = 'room_chat_' + idUser +'_'+ target;
      // }
      // join to room
      socket.join(roomName);
      
    })

  socket.on("chat:left_room",
    (payload: Payload) => {
      const { roomId, type } = payload.data;
      let roomName = 'room_chat_' + type +'_'+ roomId;
      // join to room
      socket.leave(roomName);
    })
  
    socket.on("chat:send_message",
    (payload: Payload) => {
      sendMessage(token, payload, (res: any) => {
        console.log("AAA: " + res);
        
        if (res != null && res.data) {
          const { roomId, type } = payload.data;
          let roomName = 'room_chat_' + type +'_'+ roomId;
          socket.broadcast.to(roomName).emit('chat:user_send_message', JSON.stringify({ ...res }));
        } else {
          //socket.emit('room:join_fail', '')
        }
      });
    })
  

};

