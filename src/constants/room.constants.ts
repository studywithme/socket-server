export const API_ROOM_PATH = {
    ROOM: '/rooms',
    NEW_ROOM: '/room/create',
    JOIN_ROOM: '/room/join',
    LEFT_ROOM: '/room/left'
}

export interface Room {
    id: string,
    name: string,
    currentUser: number,
    background: string
}