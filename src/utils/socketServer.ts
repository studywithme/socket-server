
// import { logger } from './logger';
import { Server as HttpServer } from "http";
import { Server, ServerOptions, Socket } from "socket.io";
import { ChatEvent } from "../events/chat.event";
import { RoomEvent } from "../events/room.event";

const connectInfo = (socket: Socket) => {
  return {
    socketId: socket.id,
    // space: socket.nsp.name,
    // User: socket.data.user,
  };
};

export function createSocketServer(
  httpServer: HttpServer,
  serverOptions: Partial<ServerOptions> = {}
): Server {
  const io = new Server(httpServer, serverOptions);

  io.on("connection", (socket: Socket) => {
    RoomEvent(socket, io);
    ChatEvent(socket, io);
    // disconnect
    socket.on("disconnect", () => {
      // logger.info('Disconnect Notifications:', connectInfo(socket));
    });
    //handler error
    socket.on("error", (err) => {
      // logger.error('Notifications:', connectInfo(socket), err);
    });
    socket.on("connect_error", (err) => {
      // logger.error('Notifications - Connect error:', connectInfo(socket), err);
    });
  });

  return io;
}
